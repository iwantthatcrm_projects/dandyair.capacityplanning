﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWNTCRM.DANDYAIR.Entities
{
    public class Job
    {
 
        public string OpID { get; set; }
        public string JobID { get; set; }
        public string ProductCategory { get; set; }
        public string Suburb { get; set; }
        public DateTime ScheduledDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public string Process { get; set; }
        public int? ProcessID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Installer { get; set; }
        public int? InstallerID { get; set; }
        public InstallerHours insHours { get; set; }

        public Job()
        {
            insHours = new InstallerHours();
        }
    }


}
