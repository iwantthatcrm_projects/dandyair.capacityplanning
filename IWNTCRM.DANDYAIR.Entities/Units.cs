﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace IWNTCRM.DANDYAIR.Entities
{
    public class Units
    {

        public int ID { get; set; }

        public SelectList NoS { get; set; }

        [Required]
        public string Unit { get; set; }

        [Required]
        [DisplayName("Number of Stories")]
        [Range(0, int.MaxValue, ErrorMessage = "Invalid entry")]
        public short NofStories { get; set; }

        [Required]
        [DisplayName("Number of Points")]
        [Range(0, int.MaxValue, ErrorMessage = "Invalid entry")]
        public int NofPotints { get; set; }

        [Required]
        [DisplayName("Number of Zones")]
        [Range(0, int.MaxValue, ErrorMessage = "Invalid entry")]
        public int NofZones { get; set; }

        public Units()
        {
            NoS = new SelectList(
                new List<SelectListItem>
                {
                    new SelectListItem {Text = "Single", Value = "1"},
                    new SelectListItem {Text = "Double", Value = "2"},
                }, "Value", "Text");
        }
    }


}
