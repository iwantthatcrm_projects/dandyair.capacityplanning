﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWNTCRM.DANDYAIR.Entities
{
    public class InstallerJobs
    {
        public int? InsID { get; set; }
        public string InstallerName { get; set; }
        public List<Job> Jobs { get; set; }
        public int DefaultApprentices { get; set; }
        public int DefaultFully_Experienced { get; set; }
        public int Apprentices { get; set; }
        public int Fully_Experienced { get; set; }
        public double TotalHours { get; set; }
        public double GrandTotal { get; set; }
    }
}
