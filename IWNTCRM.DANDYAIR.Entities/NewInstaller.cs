﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWNTCRM.DANDYAIR.Entities
{
    public class NewInstaller
    {
        public string _clientid { get; set; }
        public string _jobid { get; set; }
        public string _prevInstaller { get; set; }
        public string _newInstaller { get; set; }

    }
}
