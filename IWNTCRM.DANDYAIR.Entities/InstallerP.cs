﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWNTCRM.DANDYAIR.Entities
{
    public class InstallerP
    {
        public int? Type_Id { get; set; }
        public int? Code_Id { get; set; }
        public int? InsId { get; set; }
        public string installer { get; set; }
        public bool Checked { get; set; }
    }
}
