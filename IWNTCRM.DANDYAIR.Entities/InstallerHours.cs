﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWNTCRM.DANDYAIR.Entities
{
    public class InstallerHours
    {
        public string Unit { get; set; }

        public int Point { get; set; }

        public int NumberofStories { get; set; }

        public int NumberofZones { get; set; }

        public double RINHours { get; set; }

        public double FOHours { get; set; }

        public double CBHours { get; set; }

        public double totalHours { get; set; }
    }
}
