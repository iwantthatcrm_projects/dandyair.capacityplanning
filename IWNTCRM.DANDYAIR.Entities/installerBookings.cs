﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWNTCRM.DANDYAIR.Entities
{
    public class InstallerBookings
    {
        public int InsID { get; set; }
        public string InsName { get; set; }
        public List<Booking> Bookings { get; set; }
    }
}
