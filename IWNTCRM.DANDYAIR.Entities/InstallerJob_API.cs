﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWNTCRM.DANDYAIR.Entities
{
    public class InstallerJob_API
    {
        public string RecordID { get; set; }
        public string InstallerID { get; set; }
        public string InstallerEmail { get; set; }
        public string InstallerName { get; set; }
        public decimal Sequence_Number { get; set; }
        public bool IsCompleted { get; set; }
        public string OpID { get; set; }
        public string JobID { get; set; }
        public string JobType { get; set; }
        public string Builder { get; set; }
        public string SupervisorEmail { get; set; }
        public string SupervisorName { get; set; }
        public string BuilderSupervisor { get; set; }
        public List<string> ProductCategories { get; set; }
        public string Product { get; set; }
        public DateTime? ScheduledDatefitOff { get; set; }
        public DateTime? CompletionDatefitOff { get; set; }
        public DateTime? ScheduledDateRoughin { get; set; }
        public DateTime? CompletionDateRoughin { get; set; }
        public DateTime? ScheduledDateCandB { get; set; }
        public DateTime? CompletionDateCandB { get; set; }
        public string ScheduledTimeCandB { get; set; }
        public string Status { get; set; }
        public string Process { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Suburb { get; set; }
        public string Postcode { get; set; }
        public string City { get; set; }


        public string Job_Address { get; set; }
        public string Job_Builder { get; set; }
        public string IS_Add_on { get; set; }
        public string IS_Heating { get; set; }
        public string IS_Evap { get; set; }
        public string IS_CandB { get; set; }
        public string Installer { get; set; }

        public List<CandBUnit> HeatingUnits { get; set; }
        public List<CandBUnit> EvapUnits { get; set; }
    }

    public class UpdateJob
    {
        public string OpID { get; set; }
        public List<InstallerJob_API> JobList { get; set; }
    }

    public class CandBUnit
    {
        public string Brand { get; set; }
        public string  Model { get; set; }
        public string SerialNumber { get; set; }
    }

}
