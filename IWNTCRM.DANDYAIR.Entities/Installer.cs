﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWNTCRM.DANDYAIR.Entities
{
    public class Installer
    {
        public int InsId { get; set; }
        public int Code_Id { get; set; }

        [DisplayName("Installer")]
        public string InstallerName { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Invalid entry")]
        public int Apprentices { get; set; }

        [DisplayName("Hours per Apprentice")]
        [Range(0, double.MaxValue, ErrorMessage = "Invalid entry")]
        public double Hours_per_Apprentice { get; set; }

        [DisplayName("Fully Experienced")]
        [Range(0, int.MaxValue, ErrorMessage = "Invalid entry")]
        public int Fully_Experienced { get; set; }

        [DisplayName("Hours per Experienced")]
        [Range(0, double.MaxValue, ErrorMessage = "Invalid entry")]
        public double Hours_per_Experienced { get; set; }
    }
}
