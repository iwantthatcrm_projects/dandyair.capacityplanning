﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWNTCRM.DANDYAIR.Entities
{
    public class Process
    {
        public int? Type_Id { get; set; }
        public int? Code_Id { get; set; }
        public string Description { get; set; }
        public bool Checked { get; set; }
    }
}
