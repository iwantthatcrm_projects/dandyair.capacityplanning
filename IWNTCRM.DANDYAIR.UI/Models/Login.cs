﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IWNTCRM.DANDYAIR.UI.Models
{
    public class Login
    {
        [Required]
        [DisplayName("User Name")]
        public string username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string password { get; set; }

        [HiddenInput]
        public string ReturnUrl { get; set; }

        public bool RememberMe { get; set; }
    }
    public class CustomSerializeModel
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<string> RoleName { get; set; }

    }
}