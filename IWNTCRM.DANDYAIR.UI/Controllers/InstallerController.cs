﻿using IWNTCRM.DANDYAIR.DataRepository;
using IWNTCRM.DANDYAIR.DataRepository.ORM;
using IWNTCRM.DANDYAIR.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IWNTCRM.DANDYAIR.UI.Controllers
{
    [CustomAuthorize(Roles = "admin,staff")]
    public class InstallerController : Controller
    {
        private JobsRepo jobj;
        public InstallerController()
        {
            jobj = new JobsRepo();
        }
        public ActionResult ViewInstallers()
        {
            return View();
        }
        public ActionResult PopulateData()
        {
            DANDYAIREntities entities = new DANDYAIREntities();
            var obj = entities.Installers_tbl.ToList().OrderBy(x => x.InstallerName);
            var InsCods = obj.Select(x => x.Code_Id).ToList();
            return Json(new { data = obj, codsData = InsCods });
        }

        public JsonResult DeleteInstaller(int rawID)
        {
            using (DANDYAIREntities entities = new DANDYAIREntities())
            {
                Installers_tbl insObj = (from c in entities.Installers_tbl
                                         where c.InsId == rawID
                                         select c).FirstOrDefault();
                entities.Installers_tbl.Remove(insObj);
                entities.SaveChanges();
            }
            return Json(rawID);
        }

        public ActionResult AddNewInstaller(List<int?> InsCods)
        {
            ViewBag.InstallerList = jobj.ReturnAllInstallers().Where(v=> !InsCods.Contains(v.Code_Id) ).OrderBy(c=>c.installer).Select(x=> new KeyValuePair<int?, string>(x.Code_Id, x.installer)).ToList();
            return PartialView("_AddInstaller");
        }

        [HttpPost]
        public JsonResult InsertInstaller(Installer insObj)
        {
            try
            {
                using (DANDYAIREntities entities = new DANDYAIREntities())
                {
                    Installers_tbl obj = new Installers_tbl
                    {
                        InstallerName = insObj.InstallerName,
                        Code_Id = insObj.Code_Id,
                        DefaultApprentices = insObj.Apprentices,
                        DefaultFully_Experienced = insObj.Fully_Experienced,
                        Hours_per_Apprentice = insObj.Hours_per_Apprentice,
                        Hours_per_Experienced = insObj.Hours_per_Experienced
                    };
                    entities.Installers_tbl.Add(obj);
                    entities.SaveChanges();
                    return Json(new { result = "Successful" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {


                return Json(new { result = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult EditInstaller(int rawID)
        {
            DANDYAIREntities entities = new DANDYAIREntities();
            Installers_tbl insObj = (from c in entities.Installers_tbl
                                     where c.InsId == rawID
                                     select c).FirstOrDefault();
            Installer obj = new Installer
            {
                InstallerName = insObj.InstallerName,
                InsId = insObj.InsId,
                Apprentices = insObj.DefaultApprentices,
                Fully_Experienced = insObj.DefaultFully_Experienced,
                Hours_per_Apprentice = insObj.Hours_per_Apprentice,
                Hours_per_Experienced = insObj.Hours_per_Experienced
            };
            return PartialView("_EditInstaller", obj);
        }

        [HttpPost]
        public JsonResult UpdateInstaller(Installer installer)
        {
            try
            {
                using (DANDYAIREntities entities = new DANDYAIREntities())
                {
                    Installers_tbl updatedInstaller = (from c in entities.Installers_tbl
                                                       where c.InsId == installer.InsId
                                                       select c).FirstOrDefault();
                    updatedInstaller.DefaultApprentices = installer.Apprentices;
                    updatedInstaller.DefaultFully_Experienced = installer.Fully_Experienced;
                    updatedInstaller.Hours_per_Apprentice = installer.Hours_per_Apprentice;
                    updatedInstaller.Hours_per_Experienced = installer.Hours_per_Experienced;
                    entities.SaveChanges();
                }
                return Json(new { result = "Successful" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}