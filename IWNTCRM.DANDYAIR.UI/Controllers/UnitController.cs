﻿using IWNTCRM.DANDYAIR.DataRepository.ORM;
using IWNTCRM.DANDYAIR.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IWNTCRM.DANDYAIR.UI.Controllers
{
    [CustomAuthorize(Roles = "admin,staff")]
    public class UnitController : Controller
    {

        #region Installer CRUD
        public ActionResult ViewUnits()
        {
            return View();
        }

        public ActionResult PopulateData()
        {
            DANDYAIREntities entities = new DANDYAIREntities();
            return Json(new { data = entities.UnitsData_tbl.ToList().OrderBy(x=> x.u_ID)});
        }

        public ActionResult AddNewUnit()
        {
            Units newUniObj = new Units();
            return PartialView("_AddUnits", newUniObj);
        }

        [HttpPost]
        public ActionResult InsertUnit(Units insObj)
        {
            using (DANDYAIREntities entities = new DANDYAIREntities())
            {
                UnitsData_tbl obj= new UnitsData_tbl
                {
                    Unit = insObj.Unit,
                    NofPotints = insObj.NofPotints,
                    NofStories = insObj.NofStories,
                    NofZones = (Int16)insObj.NofZones,
                };
                entities.UnitsData_tbl.Add(obj);
                entities.SaveChanges();
            }

            return RedirectToAction("ViewUnits");
        }

        public ActionResult EditUnit(int rawID)
        {
            DANDYAIREntities entities = new DANDYAIREntities();
            UnitsData_tbl insObj = (from c in entities.UnitsData_tbl
                                        where c.u_ID == rawID
                                        select c).FirstOrDefault();
            Units obj = new Units
            {
                ID= insObj.u_ID,
                Unit = insObj.Unit,
                NofPotints = insObj.NofPotints,
                NofStories = insObj.NofStories,
                NofZones = insObj.NofZones
            };
            return PartialView("_EditUnits", obj);
        }

        [HttpPost]
        public JsonResult UpdateUnit(Units Units)
        {
            try
            {
                using (DANDYAIREntities entities = new DANDYAIREntities())
                {
                    UnitsData_tbl updatedInstaller = (from c in entities.UnitsData_tbl
                                                       where c.u_ID == Units.ID
                                                       select c).FirstOrDefault();
                    updatedInstaller.Unit = Units.Unit;
                    updatedInstaller.NofPotints = Units.NofPotints;
                    updatedInstaller.NofStories = Units.NofStories;
                    updatedInstaller.NofZones = (Int16)Units.NofZones;
                    entities.SaveChanges();
                }
                return Json(new { result = "Successful" },JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { result = ex.Message },JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteUnit(int rawID)
        {
            using (DANDYAIREntities entities = new DANDYAIREntities())
            {
                UnitsData_tbl insObj = (from c in entities.UnitsData_tbl
                                        where c.u_ID == rawID
                                         select c).FirstOrDefault();
                entities.UnitsData_tbl.Remove(insObj);
                entities.SaveChanges();
            }
            return Json(rawID);
        }

        #endregion
    }
}