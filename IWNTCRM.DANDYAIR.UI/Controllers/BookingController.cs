﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IWNTCRM.DANDYAIR.DataRepository;
using IWNTCRM.DANDYAIR.DataRepository.ORM;
using IWNTCRM.DANDYAIR.Entities;

namespace IWNTCRM.DANDYAIR.UI.Controllers
{
    [CustomAuthorize(Roles = "admin,staff")]
    public class BookingController : Controller
    {
        private JobsRepo jobj;

        public BookingController()
        {
            jobj = new JobsRepo();
        }

        public ActionResult ViewBookings()
        {
            return View();
        }

        public ActionResult GetJobs(string tDate)
        {
            DateTime selectedDate = DateTime.ParseExact(tDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            List<InstallerJobs> allJobs = new List<InstallerJobs>();
            allJobs = jobj.Get_AllJobsData_Repo(selectedDate);
            return PartialView("_GetJobs", allJobs);
        }

        public JsonResult FillProcesses(string tDate)
        {
           //DateTime selectedDate = DateTime.ParseExact(tDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
           // List<Process> proList = jobj.ReturnProcesses(selectedDate);
            List<Process> proList = jobj.ReturnProcesses();
            return Json(new { pList = proList }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FillInsatllers(string tDate)
        {
            DateTime selectedDate = DateTime.ParseExact(tDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            List<InstallerP> insList = jobj.ReturnInstallers(selectedDate);
            return Json(new { iList = insList }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveChanges(List<NewInstaller> chIns)
        {
            foreach (NewInstaller nins in chIns)
            {
                jobj.UpdateInstaller(nins._newInstaller,nins._clientid);
            }
            return Json(new { result = "1" });
        }

        public JsonResult UpdateActualManning_ForApprentices(int code_id, int value)
        {
            jobj.UpdateActualManning_ForApprentices(code_id, value);
            double grandTotal = jobj.GetGrandTotal(code_id);
            return Json(new { gTotal = grandTotal });
        }

        public JsonResult UpdateActualManning_ForFullyExperienced(int code_id, int value)
        {
            jobj.UpdateActualManning_ForFullyExperienced(code_id, value);
            double grandTotal = jobj.GetGrandTotal(code_id);
            return Json(new { gTotal = grandTotal });
        }
    }
}