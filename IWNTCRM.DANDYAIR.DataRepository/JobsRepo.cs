﻿using IWNTCRM.DANDYAIR.DataRepository.ORM;
using IWNTCRM.DANDYAIR.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWNTCRM.DANDYAIR.DataRepository
{
    public class JobsRepo
    {
        private DANDYAIREntities entities;

        public JobsRepo()
        {
            entities = new DANDYAIREntities();
        }

        public List<InstallerJobs> Get_AllJobsData_Repo(DateTime date)
        {

            List<Job> allJobs = new List<Job>();
            List<string> Client_Ids = GetClientID_By_Date(date);
            foreach (string client_id in Client_Ids)
            {
                Job job = new Job();

                //if (Installers == null)
                //{

                //}
                //else
                //{
                //var instList = Installers.Select(s => s.ToLowerInvariant()).ToList();
                var insName = GetInstaller_By_Client_Id(client_id);
                //if (insName != null && instList.Contains(insName.Code_Id.ToString()))
                if (insName != null)
                {

                    job.OpID = client_id;
                    job.JobID = GetJobIds_By_Client_Id(client_id).JobID;
                    var ProCatList = GetProductCategory_By_Client_Id(client_id);
                    if (ProCatList != null)
                    {
                        job.ProductCategory = String.Join(", ", ProCatList.Select(i => i.Product_Category).ToArray());
                    }
                    var proRes = GetProcess_By_Client_Id(client_id);
                    job.Process = (proRes == null) ? null : proRes.Process;
                    job.ProcessID = (proRes == null) ? null : proRes.Code_Id;
                    job.ScheduledDate = date;
                    job.Installer = insName.Installer;
                    job.InstallerID = insName.Code_Id;
                    job.CompletionDate = (GetCompletionDate_By_Client_Id(client_id) == null) ? null : GetCompletionDate_By_Client_Id(client_id).DateCol;
                    var result = GetClinetInfo_By_Client_Id(client_id);
                    job.Address1 = result.Address_Line_1;
                    job.Address2 = result.Address_Line_2;
                    job.Suburb = result.State_Province;
                    job.City = result.City;
                    allJobs.Add(CalCapacity(job));
                }
                else
                {
                    var aaa = GetJobIds_By_Client_Id(client_id);
                }

                //if (option == null)
                //    {
           
                //    }
                //    else
                //    {
                //        var processVal = (GetProcess_By_Client_Id(client_id).Process == null) ? string.Empty : GetProcess_By_Client_Id(client_id).Process;
                //        var optonList = option.Select(s => s.ToLowerInvariant()).ToList();
                //        if (optonList.Contains(processVal.ToLower()))
                //        {
                //            var insName = GetInstaller_By_Client_Id(client_id);
                //        //if (insName != null && instList.Contains(insName.Code_Id.ToString()))
                //        if (insName != null)
                //        {

                //            job.OpID = client_id;
                //                job.JobID = GetJobIds_By_Client_Id(client_id).JobID;
                //                var ProCatList = GetProductCategory_By_Client_Id(client_id);
                //                if (ProCatList != null)
                //                {
                //                    job.ProductCategory = String.Join(", ", ProCatList.Select(i => i.Product_Category).ToArray());
                //                }
                //                var proRes = GetProcess_By_Client_Id(client_id);
                //                job.Process = (proRes == null) ? null : proRes.Process;
                //                job.ProcessID = (proRes == null) ? null : proRes.Code_Id;
                //                job.ScheduledDate = date;
                //                job.Installer = insName.Installer;
                //                job.InstallerID = insName.Code_Id;
                //                job.CompletionDate = (GetCompletionDate_By_Client_Id(client_id) == null) ? null : GetCompletionDate_By_Client_Id(client_id).DateCol;
                //                var result = GetClinetInfo_By_Client_Id(client_id);
                //                job.Address1 = result.Address_Line_1;
                //                job.Address2 = result.Address_Line_2;
                //                job.Suburb = result.State_Province;
                //                job.City = result.City;
                //                allJobs.Add(job);
                //            }
                //            else
                //            {
                //                var aaa = GetJobIds_By_Client_Id(client_id);
                //            }
                //        }
                //    }




                //}

            }

            //if (Installers != null)
            //{
            //    var instList = Installers.Select(s => s.ToLowerInvariant()).ToList();

            //    foreach (usp_GetInstaller_Result ins in GetInstallers())
            //    {
            //        if (!allJobs.Any(inst => inst.Installer == ins.Description) && instList.Contains(ins.Description.ToLower()))
            //        {
            //            Job job = new Job();
            //            job.Installer = ins.Description;
            //            allJobs.Add(job);
            //        }
            //    }
            //}

            foreach (usp_GetInstaller_Result ins in GetInstallers())
            {
                if (!allJobs.Any(inst => inst.Installer == ins.Description))
                {
                    Job job = new Job();
                    job.Installer = ins.Description;
                    job.InstallerID = ins.Code_Id;
                    allJobs.Add(job);
                }
            }

            //InstallerJobList inJList = new InstallerJobList();
            //inJList.installerJobs = allJobs.GroupBy(u => u.Installer).Select(e => new InstallerJobs
            //{
            //    InstallerName = e.Key,
            //    Jobs = e.ToList(),
            //}).ToList();

            //inJList.Processes = inJList.installerJobs.SelectMany(x => x.Jobs).Where(c=> c.Process != null).GroupBy(v => v.Process).Select(s => new Process() { Description = CultureInfo.InvariantCulture.TextInfo.ToTitleCase(s.First().Process)}).ToList();

            List<InstallerJobs> groupInsList = allJobs.GroupBy(u => new { u.Installer, u.InstallerID }).Select(e => new InstallerJobs
            {
                InstallerName = e.Key.Installer,
                InsID = e.Key.InstallerID,
                Jobs = e.ToList(),
                TotalHours = e.Sum(v=>v.insHours.totalHours),
            }).ToList();

            foreach (InstallerJobs item in groupInsList)
            {
                var insRes = Get_GrandTotal_By_Installer(item.InsID);
                if (insRes != null)
                {
                    item.GrandTotal = insRes.GrandTotal??0;
                    item.Apprentices = insRes.Apprentices??0;
                    item.Fully_Experienced = insRes.Fully_Experienced??0;
                    item.DefaultApprentices = insRes.DefaultApprentices;
                    item.DefaultFully_Experienced = insRes.DefaultFully_Experienced;
                }
            }

            return groupInsList;
        }
  
        public List<InstallerP> ReturnAllInstallers()
        {
            return GetInstallers().Select(x => new InstallerP() { installer = x.Description, Code_Id = x.Code_Id }).ToList();
        }

        public List<InstallerP> ReturnInstallers(DateTime date)
        {
            List<InstallerP> insList = new List<InstallerP>();
            List<usp_GetInstaller_By_Client_Id_Result> avaInsList = new List<usp_GetInstaller_By_Client_Id_Result>();

            List<string> Client_Ids = GetClientID_By_Date(date);
            foreach (string client_id in Client_Ids)
            {
                avaInsList.Add(GetInstaller_By_Client_Id(client_id));
            }

            insList = GetInstallers().Select(x => new InstallerP() { installer = x.Description, Code_Id = x.Code_Id }).ToList();

            foreach (var item in avaInsList)
            {
                insList.Where(c => c.Code_Id == item.Code_Id).FirstOrDefault().Checked = true;
            }
   
            return insList;
        }

        public List<Process> ReturnProcesses()
        {
            return GetAllProcesses().Select(x => new Process { Code_Id = x.Code_Id, Type_Id = x.Type_Id, Description = CultureInfo.InvariantCulture.TextInfo.ToTitleCase(x.Description) }).ToList();
        }

        public Job CalCapacity(Job jobj)
        {
            var product = GetProduct_By_Client_Id(jobj.OpID);
            if (product != null)
            {
                List<CapacityValues_tbl> cvList = Get_CapacityValues();


                jobj.insHours.Unit = product.Unit;
                jobj.insHours.Point = product.NofPotints;
                jobj.insHours.NumberofStories = product.NofStories;


                if (jobj.insHours.NumberofStories == 1)
                {
                    jobj.insHours.RINHours = product.Time_Hrs + (jobj.insHours.Point * cvList.Where(x => x.val_Id == 1).Select(c => c.Value).FirstOrDefault()) + (jobj.insHours.NumberofZones * cvList.Where(x => x.val_Id == 6).Select(c => c.Value).FirstOrDefault());
                    jobj.insHours.CBHours = cvList.Where(x => x.val_Id == 4).Select(c => c.Value).FirstOrDefault() + (jobj.insHours.NumberofZones * cvList.Where(x => x.val_Id == 7).Select(c => c.Value).FirstOrDefault());
                }
                else
                {
                    jobj.insHours.RINHours = product.Time_Hrs + (jobj.insHours.Point * cvList.Where(x => x.val_Id == 2).Select(c => c.Value).FirstOrDefault()) + (jobj.insHours.NumberofZones * cvList.Where(x => x.val_Id == 6).Select(c => c.Value).FirstOrDefault());
                    jobj.insHours.CBHours = cvList.Where(x => x.val_Id == 5).Select(c => c.Value).FirstOrDefault() + (jobj.insHours.NumberofZones * cvList.Where(x => x.val_Id == 7).Select(c => c.Value).FirstOrDefault());
                }
                jobj.insHours.FOHours = jobj.insHours.Point * cvList.Where(x => x.val_Id == 3).Select(c => c.Value).FirstOrDefault();
    

                jobj.insHours.totalHours = jobj.insHours.RINHours + jobj.insHours.FOHours + jobj.insHours.CBHours;
            }
            return jobj;
        }

        public int UpdateActualManning_ForApprentices(int code_id, int value)
        {
            using (DANDYAIREntities entities = new DANDYAIREntities())
            {
                Installers_tbl updatedInstaller = (from c in entities.Installers_tbl
                                                   where c.Code_Id == code_id
                                                   select c).FirstOrDefault();
                if (updatedInstaller != null)
                {
                    updatedInstaller.ActualApprentices = value;
                    entities.SaveChanges();
                }

            }
            return 0;
        }

        public int UpdateActualManning_ForFullyExperienced(int code_id, int value)
        {
            using (DANDYAIREntities entities = new DANDYAIREntities())
            {
                Installers_tbl updatedInstaller = (from c in entities.Installers_tbl
                                                   where c.Code_Id == code_id
                                                   select c).FirstOrDefault();
                if (updatedInstaller != null)
                {
                    updatedInstaller.ActualFully_Experienced = value;
                    entities.SaveChanges();
                }
            }
            return 0;
        }

        public double GetGrandTotal(int code_id)
        {
            var result = Get_GrandTotal_By_Installer(code_id);
            if (result != null)
                return result.GrandTotal ?? 0;
            else
                return 0;
        }

        public bool IsUserValid(string username,string password)
        {
            CapacityPUser_tbl Obj = (from c in entities.CapacityPUser_tbl
                                     where c.Email == username && c.IsActive == 1
                                     select c).FirstOrDefault();
            if(Obj != null)
            {
                if (Security.Decrypt(Obj.Password) == password)
                    return true;
                else
                    return false;
            }
            return false;
        }

        public List<CapacityPRole_tbl> GetUserRole(string username)
        {
            CapacityPUser_tbl Obj = (from c in entities.CapacityPUser_tbl
                                     where c.Email == username
                                     select c).FirstOrDefault();

            List<CapacityPRole_tbl> roles = (from c in entities.CapacityPRole_tbl
                                             where c.RoleId == Obj.RoleId
                                             select c).ToList();
            return roles;
        }

        public List<CapacityPRole_tbl> GetUserRoles()
        {
            List<CapacityPRole_tbl> roles = (from c in entities.CapacityPRole_tbl select c).ToList();
            return roles;
        }

        public List<string> GetClientID_By_Date(DateTime date)
        {
            List<string> CliIds = new List<string>();
            ObjectParameter retVal = new ObjectParameter("retVal", 0);
            ObjectParameter retMsg = new ObjectParameter("retMsg", 0);
            return entities.usp_GetClientID_By_Date(date, retVal, retMsg).ToList();
        }

        public usp_GetJobIds_By_Client_Id_Result GetJobIds_By_Client_Id(string Client_Id)
        {
            ObjectParameter retVal = new ObjectParameter("retVal", 0);
            ObjectParameter retMsg = new ObjectParameter("retMsg", 0);
            return entities.usp_GetJobIds_By_Client_Id(Client_Id, retVal, retMsg).FirstOrDefault();
        }

        public usp_GetInstaller_By_Client_Id_Result GetInstaller_By_Client_Id(string Client_Id)
        {
            ObjectParameter retVal = new ObjectParameter("retVal", 0);
            ObjectParameter retMsg = new ObjectParameter("retMsg", 0);
            return entities.usp_GetInstaller_By_Client_Id(Client_Id, retVal, retMsg).FirstOrDefault();
        }

        public List<usp_GetInstaller_Result> GetInstallers()
        {
            ObjectParameter retVal = new ObjectParameter("retVal", 0);
            ObjectParameter retMsg = new ObjectParameter("retMsg", 0);
            return entities.usp_GetInstaller(retVal, retMsg).ToList();
        }

        public usp_GetProcess_By_Client_Id_Result GetProcess_By_Client_Id(string Client_Id)
        {
            ObjectParameter retVal = new ObjectParameter("retVal", 0);
            ObjectParameter retMsg = new ObjectParameter("retMsg", 0);
            return entities.usp_GetProcess_By_Client_Id(Client_Id, retVal, retMsg).FirstOrDefault();
        }

        public List<usp_GetProductCategory_By_Client_Id_Result> GetProductCategory_By_Client_Id(string Client_Id)
        {
            ObjectParameter retVal = new ObjectParameter("retVal", 0);
            ObjectParameter retMsg = new ObjectParameter("retMsg", 0);
            return entities.usp_GetProductCategory_By_Client_Id(Client_Id, retVal, retMsg).ToList();
        }

        public usp_GetCompletionDate_By_Client_Id_Result GetCompletionDate_By_Client_Id(string Client_Id)
        {
            ObjectParameter retVal = new ObjectParameter("retVal", 0);
            ObjectParameter retMsg = new ObjectParameter("retMsg", 0);
            return entities.usp_GetCompletionDate_By_Client_Id(Client_Id, retVal, retMsg).FirstOrDefault();
        }

        public usp_GetClinetInfo_By_Client_Id_Result GetClinetInfo_By_Client_Id(string Client_Id)
        {
            ObjectParameter retVal = new ObjectParameter("retVal", 0);
            ObjectParameter retMsg = new ObjectParameter("retMsg", 0);
            return entities.usp_GetClinetInfo_By_Client_Id(Client_Id, retVal, retMsg).FirstOrDefault();
        }

        public List<usp_GetProcesses_Result> GetAllProcesses()
        {
            ObjectParameter retVal = new ObjectParameter("retVal", 0);
            ObjectParameter retMsg = new ObjectParameter("retMsg", 0);
            return entities.usp_GetProcesses(retVal, retMsg).ToList();
        }

        public usp_GetProduct_By_Client_Id_Result GetProduct_By_Client_Id(string Client_Id)
        {
            ObjectParameter retVal = new ObjectParameter("retVal", 0);
            ObjectParameter retMsg = new ObjectParameter("retMsg", 0);
            return entities.usp_GetProduct_By_Client_Id(Client_Id, retVal, retMsg).FirstOrDefault();
        }

        public usp_Get_GrandTotal_By_Installer_Result Get_GrandTotal_By_Installer(int? installerID)
        {
            ObjectParameter retVal = new ObjectParameter("retVal", 0);
            ObjectParameter retMsg = new ObjectParameter("retMsg", 0);
            return entities.usp_Get_GrandTotal_By_Installer(installerID, retVal, retMsg).FirstOrDefault();
        }

        public int UpdateInstaller(string installer, string clientID)
        {
            return entities.usp_UpdateInstaller(installer, clientID);
        }

        public List<CapacityValues_tbl> Get_CapacityValues()
        {
            return entities.CapacityValues_tbl.ToList();
        }     
    }
}
