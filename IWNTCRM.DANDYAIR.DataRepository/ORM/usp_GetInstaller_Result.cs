//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IWNTCRM.DANDYAIR.DataRepository.ORM
{
    using System;
    
    public partial class usp_GetInstaller_Result
    {
        public Nullable<int> Type_Id { get; set; }
        public Nullable<int> Code_Id { get; set; }
        public string Description { get; set; }
    }
}
