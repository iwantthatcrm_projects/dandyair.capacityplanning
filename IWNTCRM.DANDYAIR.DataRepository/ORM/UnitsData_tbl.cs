//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IWNTCRM.DANDYAIR.DataRepository.ORM
{
    using System;
    using System.Collections.Generic;
    
    public partial class UnitsData_tbl
    {
        public int u_ID { get; set; }
        public short NofStories { get; set; }
        public string Unit { get; set; }
        public int NofPotints { get; set; }
        public short NofZones { get; set; }
    }
}
